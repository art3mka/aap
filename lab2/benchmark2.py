import cProfile
import pstats
import lab2.library2
from lab1.utility1 import line_with_calls, graphic
from lab2.utility2 import create_array


first = 4 * 2
last = 4 * 20
step = 4 * 2

array_len = [i for i in range(first, last, step)]
operations = []

func = ['lab2.library2.linear_search(arrays[0], -1)',
        'lab2.library2.binary_search(arrays[0], -1)',
        'lab2.library2.naive_search(arrays[1], "KL")',
        'lab2.library2.kmp_search(arrays[1], "KL")']

for e in func:
    for i in range(first, last, step):
        arrays = create_array(i)
        cProfile.run(e, 'stats.log')
        with open('output.txt', 'w') as log_file:
            p = pstats.Stats('stats.log', stream=log_file)
            p.strip_dirs().sort_stats(pstats.SortKey.CALLS).print_stats()
        f = open('output.txt')
        line = line_with_calls(f)
        f.close()
        operations.append(int(line))
    graphic(array_len, operations, 2, 'Количество операций')
    operations = []
