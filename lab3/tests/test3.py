import networkx as nx
import lab3.library3


def test_shortest_path():
    # data loading
    graph_data = lab3.library3.graph_loader('data.txt')

    # graph create
    nx_graph = lab3.library3.get_nx_graph(graph_data)
    my_graph = lab3.library3.get_my_graph(graph_data[0], graph_data[1])

    # get shortest_path
    expected_path = nx.shortest_path(nx_graph, source='A', target='D')
    my_path = min([(len(e), e) for e in lab3.library3.find_all_paths(my_graph, 'A', 'D')])[1]

    assert my_path == expected_path
