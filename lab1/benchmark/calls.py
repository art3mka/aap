from lab1.utility1 import create_array, line_with_calls, graphic
import lab1.library1
import cProfile
import pstats


first = 4 * 2
last = 4 * 20
step = 4 * 2

array_len = [i for i in range(first, last, step)]
operations = []

func = ['lab1.library1.bubble_sort(array)',
        'lab1.library1.insertion_sort(array)',
        'lab1.library1.shell_sort(array)',
        'lab1.library1.quick_sort(array)']

for e in func:
    for i in range(first, last, step):
        array = create_array(i)
        cProfile.run(e, 'stats.log')
        with open('output.txt', 'w') as log_file:
            p = pstats.Stats('stats.log', stream=log_file)
            p.strip_dirs().sort_stats(pstats.SortKey.CALLS).print_stats()
        f = open('output.txt')
        line = line_with_calls(f)
        f.close()
        operations.append(int(line))
    graphic(array_len, operations, 2, 'Количество операций')
    operations = []
