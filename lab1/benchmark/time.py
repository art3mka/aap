from timeit import default_timer as timer
from lab1.utility1 import create_array, graphic
import lab1.library1


first = 4 * 2
last = 4 * 20
step = 4 * 2

array_len = [i for i in range(first, last, step)]
array_time = []

func = [lab1.library1.bubble_sort,
        lab1.library1.insertion_sort,
        lab1.library1.shell_sort,
        lab1.library1.quick_sort]

for sort in func:
    for i in range(first, last, step):
        array = create_array(i)
        start = timer()
        sort(array)
        end = timer()
        array_time.append(end - start)
    graphic(array_len, array_time, 2, 'Время, сек')
    array_time = []
