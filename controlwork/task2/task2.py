from controlwork.task1 import lib


def get_values(method, func, k, low, up, ref, intervals):
    array_values = []
    for i in intervals:
        result = method(func, k, low, up, i)
        array_values.append(abs(ref - result))
    return array_values


array_intervals = [i for i in range(100, 1000, 100)]
expected = 190.97903
a = 1
x_low = 5
x_up = 10

simpson_method = get_values(lib.simpson_method, lib.g, a, x_low, x_up, expected, array_intervals)
rectangle_method = get_values(lib.rectangle_method, lib.g, a, x_low, x_up, expected, array_intervals)
trapezoid_method = get_values(lib.trapezoid_method, lib.g, a, x_low, x_up, expected, array_intervals)

lib.graphic(array_intervals, rectangle_method, trapezoid_method, simpson_method)
