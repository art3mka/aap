from controlwork.task1.lib import simpson_method, rectangle_method, trapezoid_method, g

expected = 190.97903


def test_simpsons_method():
    result = simpson_method(g, 1, 5, 10, intervals=1000)
    assert abs(result - expected) <= 0.1


def test_rectangle_method():
    result = rectangle_method(g, 1, 5, 10, intervals=1000)
    assert abs(result - expected) <= 0.1


def test_trapezium_method():
    result = trapezoid_method(g, 1, 5, 10, intervals=1000)
    assert abs(result - expected) <= 0.1
