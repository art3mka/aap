from matplotlib import pyplot as plt
import statistics
import numpy as np

def simpson_method(func, a, lower, upper, intervals=20):
    if intervals % 2 == 1:
        intervals += 1
    step = 1.0 * (upper - lower) / intervals
    integral = (func(a, lower) + 4 * func(a, lower + step) + func(a, upper))
    for i in range(1, intervals // 2):
        integral += 2 * func(a, lower + (2 * i) * step) + 4 * func(a, lower + (2 * i + 1) * step)
    return integral * step / 3


def rectangle_method(func, a, lower, upper, intervals=20):
    step = 1.0 * (upper - lower) / intervals
    integral = 0
    for i in range(intervals):
        integral += step * func(a, lower + i * step)
    return integral


def trapezoid_method(func, a, lower, upper, intervals=20):
    step = 1.0 * (upper - lower) / intervals
    integral = 0.5 * (func(a, lower) + func(a, upper))
    for i in range(1, intervals):
        integral += func(a, lower + i * step)
    return integral * step


def g(a, x):
    num = 8 * (21 * a ** 2 + 34 * a * x + 8 * x ** 2)
    denum = 2 * a ** 2 - 9 * a * x + 4 * x ** 2
    if denum == 0:
        return None
    return num / denum


def graphic(intervals, rectangle, trapezium, simpson):
    #plt.plot(intervals, rectangle, '-or', label='Метод прямоугольников')
    plt.plot(intervals, trapezium, '-og', label='Метод трапеций')
    #plt.plot(intervals, simpson, '-b', label='Метод Симпсона')
    plt.xlabel('количество интервалов')
    plt.ylabel('разность между истинным значением и приближением')
    plt.tight_layout()
    plt.legend()
    plt.show()


def sma(data, w):
    """Simple Moving Average (SMA) Filter"""
    result = []
    for m in range(len(data) - (w - 1)):
        result.append(sum(data[m:m+w]) / w)
    return result


def ema(data, w):
    """Exponential Moving Average (EMA) Filter"""
    result = []
    alpha = 2 / (w + 1)
    for m in range(len(data) - (w - 1)):
        result.append((alpha * data[m]) + ((1 - alpha) * data[m:m+w][-1]))
    return result


def smm(data, w):
    """Simple Moving Median (SMM) Filter"""
    result = []
    for m in range(len(data) - (w - 1)):
        result.append(statistics.median(data[m:m+w]))
    return result


def sma_numpy(data, w):
    return np.convolve(data, np.ones(w), 'valid') / w
