import lib

try:
    a = float(input('Введите а: '))
    x1 = float(input('Нижний предел интегрирования: '))
    x2 = float(input('Верхний предел интегрирования: '))
except ValueError:
    print('Ошибка ввода')
    exit(1)

if x2 > x1:
    print(f'Метод Симпсона: {lib.simpson_method(lib.g, a, x1, x2):.7f}')
    print(f'Метод прямоугольников: {lib.rectangle_method(lib.g, a, x1, x2):.7f}')
    print(f'Метод трапеций: {lib.trapezoid_method(lib.g, a, x1, x2):.7f}')
else:
    print('Ошибка ввода')
