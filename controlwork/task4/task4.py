import xlrd
from controlwork.task1.lib import sma, ema, smm
from matplotlib import pyplot as plt

file = xlrd.open_workbook('data.xls', formatting_info=True)
sheet = file.sheet_by_index(0)
data = sheet.col_values(19, start_rowx=1, end_rowx=202)
data = [float(elem) for elem in data]

sma_result = sma(data, 3)
ema_result = ema(data, 3)
smm_result = smm(data, 3)

fig = plt.figure()
fig.set_size_inches(12, 10, forward=True)

plt.subplot(3, 1, 1)
plt.title('Простая скользящая средняя')
plt.plot(data, 'k', label='До выполнения')
plt.plot(sma_result, 'r', label='После выполнения')
plt.legend()

plt.subplot(3, 1, 2)
plt.title('Экспоненциальная скользящая средняя')
plt.plot(data, 'k', label='До выполнения')
plt.plot(ema_result, 'g', label='После выполнения')
plt.legend()

plt.subplot(3, 1, 3)
plt.title('Простая скользящая медиана (медианный фильтр)')
plt.plot(data, 'k', label='До выполнения')
plt.plot(smm_result, 'b', label='После выполнения')
plt.legend()

plt.tight_layout()
plt.show()
