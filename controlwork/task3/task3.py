from random import random
import matplotlib.pyplot as plt
import numpy as np


passages = list(np.linspace(10, 1000000, 1000))
pi = []


def multiple_repeats(passages, hits=0):
    for i in range(int(passages)):
        x = random()
        y = random()
        if x ** 2 + y ** 2 < 1:
            hits += 1
    return float(hits)


for i in passages:
    pi.append(4*(multiple_repeats(i)/i))
    print('trials: %d, hits: %d, pi = %1.4F' % (i, multiple_repeats(i), 4*(multiple_repeats(i)/i)))

plt.plot(passages, pi, 'r')
plt.title('Оценка значения Пи с помощью метода Монте-Карло')
plt.xlabel('Кол-во испытаний')
plt.ylabel('Примерное значение Пи')
plt.axhline(y=3.1415926, label='Pi')
plt.legend()
plt.show()


